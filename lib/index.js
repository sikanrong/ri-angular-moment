'use strict';

require("./moment");
require("./moment-timezone-with-data");
require("./locales/es");
require("./locales/de");
require("./locales/fr");